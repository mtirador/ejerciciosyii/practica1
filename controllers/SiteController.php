<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\data\SqlDataProvider; //importante las dependencias
use yii\data\ActiveDataProvider; //importante las dependencias
use app\models\Ciclista;//importante las dependencias
use app\models\Lleva;
use app\models\Equipo;
use app\models\Puerto;


class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
    
     
    public function actionConsulta1(){
        
          //mediante DAO
          //se crea un proveedor de datos con la consulta correspondiente
        $dataProvider = new \yii\data\SqlDataProvider([
            
            'sql'=>'SELECT DISTINCT edad FROM ciclista',
        ]);
        
        //se renderiza la vista donde vamos a mostrar los datos
        //le pasamos varios parámetros para usar en la vista incluyendo 
        //el proveedor de datos
        //un array con los campos que se van a visualizar en el GridView
        //algunas variables que vamos a querer usar en la vista
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=>"Consulta 1 con Dao ",
            "enunciado"=>"Listar las edades de los ciclistas (sin repetidos)",
            "sql"=>"SELECT DISTINCT edad FROM ciclista",
            
            
        ]);
    }
    
    public function actionConsulta1a(){           
         //mediante Active Record 
        $dataProvider = new ActiveDataProvider([
            'query'=> Ciclista::find()->select("edad")->distinct(),
            
        ]);
        
                return $this->render("resultado",[
                    "resultados"=>$dataProvider,
                    "campos"=>['edad'],
                    "titulo"=>"Consulta 1a con Active Record",
                    "enunciado"=>"Listar las edades de los ciclistas(sin repetidos)",
                    "sql"=>"SELECT DISTINCT edad FROM ciclista",
                    
                ]);     
    }
    
    public function actionConsulta2() {
        //mediante DAO
          //se crea un proveedor de datos con la consulta correspondiente
        $dataProvider = new \yii\data\SqlDataProvider([
            
            'sql'=>'SELECT DISTINCT edad FROM ciclista WHERE nomequipo="Artiach"',
        ]);
        
        //se renderiza la vista donde vamos a mostrar los datos
        //le pasamos varios parámetros para usar en la vista incluyendo 
        //el proveedor de datos
        //un array con los campos que se van a visualizar en el GridView
        //algunas variables que vamos a querer usar en la vista
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=>"Consulta 2 con Dao ",
            "enunciado"=>"Listar las edades de los ciclistas de Artiach",
            "sql"=>"SELECT DISTINCT edad FROM ciclista WHERE nomequipo='Artiach'",
              
        ]);
    }
    
    public function actionConsulta2a(){
  //mediante Active Record
        
        $dataProvider = new ActiveDataProvider([
            'query'=> Ciclista::find()->select("edad")->distinct()->where(['nomequipo' => 'Artiach']),
            
        ]);
        
                return $this->render("resultado",[
                    "resultados"=>$dataProvider,
                    "campos"=>['edad'],
                    "titulo"=>"Consulta 2a con Active Record",
                    "enunciado"=>"Listar las edades de los ciclistas de Artiach",
                    "sql"=>"SELECT DISTINCT edad FROM ciclista WHERE nomequipo='Artiach'",
                    
                ]);
      
    }
    
    public function actionConsulta3(){
           //Mediante Dao
        $dataProvider = new \yii\data\SqlDataProvider([
            
            'sql'=>'SELECT DISTINCT edad FROM ciclista WHERE nomequipo="Artiach" OR nomequipo="Amore Vita"',
        ]);
        
        //se renderiza la vista donde vamos a mostrar los datos
        //le pasamos varios parámetros para usar en la vista incluyendo 
        //el proveedor de datos
        //un array con los campos que se van a visualizar en el GridView
        //algunas variables que vamos a querer usar en la vista
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=>"Consulta 3 con Dao ",
            "enunciado"=>"Listar las edades de los ciclistas de Artiach o Amore Vita",
            "sql"=>"SELECT DISTINCT edad FROM ciclista WHERE nomequipo='Artiach' OR nomequipo='Amore Vita'",
              
        ]);
        
    }
    
    public function actionConsulta3a(){
         // Mediante Active Record
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()->select("edad")->distinct()
               /* ->where(['nomequipo' => ['Artiach', 'Amore Vita']]),*/ //esta es otra forma de hacerlo
                ->where ('nomequipo = "Artiach" OR nomequipo = "Amore Vita"'),
        ]);

        return $this->render("resultado", [
            "resultados" => $dataProvider,
            "campos" => ['edad'],
            "titulo" => "Consulta 3a con Active Record",
            "enunciado" => "Listar las edades de los ciclistas de Artiach o Amore Vita",
            "sql" => "SELECT DISTINCT edad FROM ciclista WHERE nomequipo='Artiach' OR nomequipo='Amore Vita'",
        ]);
        
    }
   
    public function actionConsulta4(){
        //Mediante Dao
        $dataProvider = new \yii\data\SqlDataProvider([
            
            'sql'=>'SELECT DISTINCT dorsal FROM ciclista WHERE edad < 25 OR edad > 30',
        ]);
        
        //se renderiza la vista donde vamos a mostrar los datos
        //le pasamos varios parámetros para usar en la vista incluyendo 
        //el proveedor de datos
        //un array con los campos que se van a visualizar en el GridView
        //algunas variables que vamos a querer usar en la vista
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 4 con Dao ",
            "enunciado"=>"Listar los dorsales de ciclistas con edad menor de 25 o mayor de 30",
            "sql"=>"SELECT DISTINCT dorsal FROM ciclista WHERE edad < 25 OR edad > 30",
              
        ]);
        
     
    }
    
    public function actionConsulta4a(){
       // Mediante Active Record
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()->select("dorsal")->distinct()
               /* ->where(['OR', ['<', 'edad', 25], ['>', 'edad', 30]]),*/
                ->where ('edad < 25 OR edad > 30'),
        ]);

        return $this->render("resultado", [
            "resultados" => $dataProvider,
            "campos" => ['dorsal'],
            "titulo" => "Consulta 4a con Active Record",
            "enunciado" => "Listar los dorsales de ciclistas con edad menor de 25 o mayor de 30",
            "sql" => "SELECT DISTINCT dorsal FROM ciclista WHERE edad < 25 OR edad > 30",
        ]); 
    }
    
    public function actionConsulta5(){
        //Mediante Dao
        $dataProvider = new \yii\data\SqlDataProvider([
            
            'sql'=>'SELECT DISTINCT dorsal FROM ciclista WHERE edad BETWEEN 28 AND 32 AND nomequipo="Banesto"',
        ]);
        
        //se renderiza la vista donde vamos a mostrar los datos
        //le pasamos varios parámetros para usar en la vista incluyendo 
        //el proveedor de datos
        //un array con los campos que se van a visualizar en el GridView
        //algunas variables que vamos a querer usar en la vista
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 5 con Dao ",
            "enunciado"=>"Listar los dorsales de los ciclistas cuya edad esté entre 28 y 32 y además que sólo sean de Banesto",
            "sql"=>"SELECT DISTINCT dorsal FROM ciclista WHERE edad BETWEEN 28 AND 32 AND nomequipo='Banesto'",
              
        ]);
    }
    
    public function actionConsulta5a(){
        // Mediante Active Record
        $dataProvider = new ActiveDataProvider([
        'query' => Ciclista::find()->select("dorsal")->distinct()
           /* ->where(['AND', ['BETWEEN', 'edad', 28, 32], ['nomequipo' => 'Banesto']]),*/
                ->where ('edad BETWEEN 28 AND 32 AND nomequipo="Banesto"'),
        ]);

        return $this->render("resultado", [
            "resultados" => $dataProvider,
            "campos" => ['dorsal'],
            "titulo" => "Consulta 5a con Active Record",
            "enunciado" => "Listar los dorsales de los ciclistas cuya edad esté entre 28 y 32 y además que sólo sean de Banesto",
            "sql" => "SELECT DISTINCT dorsal FROM ciclista WHERE edad BETWEEN 28 AND 32 AND nomequipo='Banesto'",
        ]); 
    }
    
    public function actionConsulta6(){
        //Mediante Dao
        $dataProvider = new \yii\data\SqlDataProvider([
            
            'sql'=>'SELECT DISTINCT nombre FROM ciclista WHERE CHAR_LENGTH(nombre)>8',
        ]);
        
        //se renderiza la vista donde vamos a mostrar los datos
        //le pasamos varios parámetros para usar en la vista incluyendo 
        //el proveedor de datos
        //un array con los campos que se van a visualizar en el GridView
        //algunas variables que vamos a querer usar en la vista
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nombre'],
            "titulo"=>"Consulta 6 con Dao ",
            "enunciado"=>"Indícame el nombre de los ciclistas que el número de caracteres del nombre sea mayor que 8",
            "sql"=>"SELECT DISTINCT nombre FROM ciclista WHERE CHAR_LENGTH(nombre)>8",
              
        ]);
    }
    
    public function actionConsulta6a(){
       // Mediante Active Record
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()->select("nombre")->distinct()
               /* ->where(['>', 'CHAR_LENGTH(nombre)', 8]),*/
                ->where('CHAR_LENGTH(nombre)>8'),
        ]);

        return $this->render("resultado", [
            "resultados" => $dataProvider,
            "campos" => ['nombre'],
            "titulo" => "Consulta 6a con Active Record",
            "enunciado" => "Indícame el nombre de los ciclistas que el número de caracteres del nombre sea mayor que 8",
            "sql" => "SELECT DISTINCT nombre FROM ciclista WHERE CHAR_LENGTH(nombre)>8",
        ]); 
    }
    
    public function actionConsulta7(){
        //Mediante Dao
        $dataProvider = new \yii\data\SqlDataProvider([
            
            'sql'=>'SELECT nombre,dorsal,UPPER(nombre) AS "Nombre Mayúsculas" FROM ciclista',
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nombre','dorsal','nombre_mayus'],
            "titulo"=>"Consulta 7 con Dao ",
            "enunciado"=>"Lístame el nombre y el dorsal de todos los ciclistas mostrando un campo nuevo denominado nombre mayúsculas que debe mostrar el nombre en mayúsculas",
            "sql"=>"SELECT nombre,dorsal,UPPER(nombre) AS 'Nombre Mayúsculas' FROM ciclista",
              
        ]);
    }
    
    public function actionConsulta7a(){
        // Mediante Active Record
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()->/*select(['nombre', 'dorsal', 'UPPER(nombre) AS nombre_mayus'])->distinct(),*/
                select('nombre,dorsal, UPPER(nombre) AS nombre_mayus')->distinct(),
            
      
        ]);

        return $this->render("resultado", [
            "resultados" => $dataProvider,
            "campos" => ['nombre','dorsal', 'nombre_mayus'], 
            "titulo" => "Consulta 7a con Active Record",
            "enunciado" => "Lístame el nombre y el dorsal de todos los ciclistas mostrando un campo nuevo denominado nombre mayúsculas que debe mostrar el nombre en mayúsculas",
            "sql" => "SELECT nombre,dorsal,UPPER(nombre) AS 'Nombre Mayúsculas' FROM ciclista",
        ]); 
    }

    
    public function actionConsulta8(){
        //Mediante Dao
        $dataProvider = new \yii\data\SqlDataProvider([
            
            'sql'=>'SELECT DISTINCT dorsal from lleva where código="MGE"',
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 8 con Dao ",
            "enunciado"=>"Listar todos los ciclistas que han ganado el maillot MGE (amarillo) en alguna etapa",
            "sql"=>"SELECT DISTINCT dorsal from lleva where código='MGE'",
              
        ]);
    }
    
   
    public function actionConsulta8a(){
            // Mediante Active Record
            $dataProvider = new ActiveDataProvider([
                'query' => \app\models\Lleva::find()->select('dorsal')->distinct()
                    ->where('código = "MGE"'),
            ]);

            return $this->render("resultado", [
                "resultados" => $dataProvider,
                "campos" => ['dorsal'],
                "titulo" => "Consulta 8a con Active Record",
                "enunciado" => "Listar todos los ciclistas que han ganado el maillot MGE (amarillo) en alguna etapa",
                "sql" => "SELECT DISTINCT dorsal FROM lleva WHERE código = 'MGE'",
            ]); 


    }
    


    
    public function actionConsulta9(){
         //Mediante Dao
        $dataProvider = new \yii\data\SqlDataProvider([
            
            'sql'=>'SELECT DISTINCT nompuerto FROM puerto WHERE altura>1500',
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nompuerto'],
            "titulo"=>"Consulta 9 con Dao ",
            "enunciado"=>"Listar el nombre de los puertos cuya altura sea mayor de 1500",
            "sql"=>"SELECT DISTINCT nompuerto FROM puerto WHERE altura>1500",
              
        ]);
    }
    
    public function actionConsulta9a(){

        // Mediante Active Record
        $dataProvider = new ActiveDataProvider([
            'query' =>\app\models\Puerto::find()->/*select(['nompuerto'])->distinct()->where(['>', 'altura', 1500]),*/
                select('nompuerto ')->distinct()
                ->where('altura>1500'),
   
        ]);

        return $this->render("resultado", [
            "resultados" => $dataProvider,
            "campos" => ['nompuerto'],
            "titulo" => "Consulta 9a con Active Record",
            "enunciado" => "Listar el nombre de los puertos cuya altura sea mayor de 1500",
            "sql" => "SELECT DISTINCT nompuerto FROM puerto WHERE altura>1500",
        ]); 
    
    }

        
    

    public function actionConsulta10(){
         // Mediante DAO
        $dataProvider = new \yii\data\SqlDataProvider([
            'sql' => 'SELECT DISTINCT dorsal FROM puerto WHERE pendiente > 8 OR (altura BETWEEN 1800 AND 3000)',
        ]);

        return $this->render("resultado", [
            "resultados" => $dataProvider,
            "campos" => ['dorsal'],
            "titulo" => "Consulta 10 con DAO",
            "enunciado" => "Listar el dorsal de los ciclistas que hayan ganado algun puerto cuya pendiente sea mayor que 8 o cuya altura esté entre 1800 y 3000",
            "sql" => "SELECT DISTINCT dorsal FROM puerto WHERE pendiente > 8 OR (altura BETWEEN 1800 AND 3000)",
        ]);
    }
    public function actionConsulta10a(){
            // Mediante Active Record
         $dataProvider = new ActiveDataProvider([
             'query' => Puerto::find()
                 ->select(['dorsal'])
                 ->distinct()
                 ->where('pendiente > 8 OR (altura BETWEEN 1800 and 3000)'),
             
                 
         ]);

         return $this->render("resultado", [
             "resultados" => $dataProvider,
             "campos" => ['dorsal'],
             "titulo" => "Consulta 10a con Active Record",
             "enunciado" => "Listar el dorsal de los ciclistas que hayan ganado algun puerto cuya pendiente sea mayor que 8 o cuya altura esté entre 1800 y 3000",
             "sql" => "SELECT DISTINCT dorsal FROM puerto WHERE pendiente > 8 OR (altura BETWEEN 1800 AND 3000)",
         ]); 
    }
    
    public function actionConsulta11(){
            // Mediante DAO
       $dataProvider = new \yii\data\SqlDataProvider([
           'sql' => 'SELECT DISTINCT dorsal FROM puerto WHERE pendiente > 8 AND (altura BETWEEN 1800 AND 3000)',
       ]);

       return $this->render("resultado", [
           "resultados" => $dataProvider,
           "campos" => ['dorsal'],
           "titulo" => "Consulta 11 con DAO",
           "enunciado" => "Listar el dorsal de los ciclistas que hayan ganado algún puerto cuya pendiente sea mayor que 8 y cuya altura esté entre 1800 y 3000",
           "sql" => "SELECT DISTINCT dorsal FROM puerto WHERE pendiente > 8 AND (altura BETWEEN 1800 AND 3000)",
       ]);
    }
    public function actionConsulta11a(){
        // Mediante Active Record
    $dataProvider = new ActiveDataProvider([
        'query' => Puerto::find()
            ->select(['dorsal'])
            ->distinct()
            ->where('pendiente >8 AND (altura BETWEEN 1800 AND 3000)'),
         ]);

    return $this->render("resultado", [
        "resultados" => $dataProvider,
        "campos" => ['dorsal'],
        "titulo" => "Consulta 11a con Active Record",
        "enunciado" => "Listar el dorsal de los ciclistas que hayan ganado algún puerto cuya pendiente sea mayor que 8 y cuya altura esté entre 1800 y 3000",
        "sql" => "SELECT DISTINCT dorsal FROM puerto WHERE pendiente > 8 AND (altura BETWEEN 1800 AND 3000)",
    ]);
    }
}
